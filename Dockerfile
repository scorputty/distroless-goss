# Start by building the application.
FROM golang:1.12 as build-env

ENV GOSS_VER v0.3.7

WORKDIR /go/src/app

# See https://github.com/aelsabbahy/goss/releases for release versions
RUN curl -L https://github.com/aelsabbahy/goss/releases/download/${GOSS_VER}/goss-linux-amd64 -o goss \
&& chmod +rx goss

# (optional) dgoss docker wrapper (use 'master' for latest version)
RUN curl -L https://raw.githubusercontent.com/aelsabbahy/goss/master/extras/dgoss/dgoss -o dgoss \
&& chmod +rx dgoss

RUN touch /root/goss.yml

# Install latest version to /usr/local/bin
# RUN curl -fsSL https://goss.rocks/install | sh

FROM gcr.io/distroless/base
ENV GOSS_PATH="/usr/local/bin"
ENV GOSS_FILE="/root/goss.yml"
COPY --from=build-env /go/src/app /usr/local/bin
COPY --from=build-env /root/goss.yml /root/goss.yml
